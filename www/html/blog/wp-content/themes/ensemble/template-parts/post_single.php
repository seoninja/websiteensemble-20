	<div class="search mt-5 mb-3">
		<div class="container">
			<div class="row  justify-content-left">
				<div class="col-12 ">
				<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/blog/">Expert Insights</a></li>
    <li class="breadcrumb-item"><?php $a= wp_get_post_categories(get_the_ID());
		
		foreach($a as $value)
		{
			$trma=get_category($value);
			
			$trm=$trma->name;
			$trmL=$trma->slug;
			echo '<a href="/blog/category/'.$trmL.'/">'.$trm.'</a> ';
		
		
		}
		
		?></li>
    <li class="breadcrumb-item active" aria-current="page"><?php the_title(); ?></li>
  </ol>
</nav>
				</div>
			</div>
		</div>
	</div>

<div class="articles">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="post">
					<div class="artImg "><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></div>
					
					<div class="artBody px-5 pb-5 pt-3">
						
					  <div class="details"><?php the_category( ' ' ); ?> <?php the_date(); ?></div>
						<h1 class="fw-bold clearStf"><?php the_title(); ?></h1>
						<div class="author" style="background-image:url(/assets/images/author<?php echo get_the_author_meta( 'ID' );?>.png)">by <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) );?>"><?php echo get_the_author(); ?></a></div>
						<div class="mt-3">
							<?php the_content();?></div>
						
						<div class="clearStf pt-5">
						<div class="justify-content-between">
							<?php previous_post_link(); ?>
							<?php next_post_link( );?>
							
						</div>
							</div>
					</div>
				
				</div>
			
			</div>
		

		</div>
	</div>
	
	
	</div>
